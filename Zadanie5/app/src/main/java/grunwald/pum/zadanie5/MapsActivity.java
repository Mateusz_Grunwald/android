package grunwald.pum.zadanie5;

import androidx.fragment.app.FragmentActivity;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Warsaw and move the camera
        LatLng warsaw = new LatLng(52.237049, 21.017532); //N/S coordinates (N=+, S=-) , E/W (E=+, W=-)
        mMap.addMarker(new MarkerOptions().position(warsaw).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(warsaw));
    }

    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.map_manu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item){ //nie wiem czy to powinno tak wyglądać
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        item.setChecked(true);
        return true;
    }
}
