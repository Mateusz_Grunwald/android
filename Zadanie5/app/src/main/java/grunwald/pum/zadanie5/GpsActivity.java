package grunwald.pum.zadanie5;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

public class GpsActivity extends AppCompatActivity implements LocationListener {
    LocationManager locationManager; //zapewnia dostęp do usługi lokalizacji
    Criteria criteria; //wskazuje na kryteria aplikacji do wyboru dostawcy lokalizacji
    Location location; //położenie geograficzne (długośc i szerokość)
    String[] perm = new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}; //lista uprawnień
    TextView heightText;
    TextView widthText;

    private boolean checkPermission(String[] permissions) { //funkcja do sprawdzania uprawnień
        if (permissions != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) { //odpowiednia wersja systemu
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED)
                    return false;
            }
        }
        return true; //jak sprawdzamy jakieś pozwolenia i wszystkie przejdą to jest true, inaczej false
    }

    private void requestPermission(String[] permissions) { // prosi o nadanie uprawnien:
        ActivityCompat.requestPermissions(this, permissions, 99); //99 to chyba kod lokacji
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gps);
        widthText = findViewById(R.id.widthText);
        heightText = findViewById(R.id.heightText);
        criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        criteria.setPowerRequirement(Criteria.POWER_LOW);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (!checkPermission(perm)) { //proszenie i udzielanie uprawnień
            requestPermission(perm);
        }
        try {
            locationManager.requestLocationUpdates(500, (float) 0.1, criteria, this, null);
            refresh();
            writeLatLong();
        }
        catch (SecurityException e) {
            Toast.makeText(this, "Nie mamy uprawnień", Toast.LENGTH_LONG);
            e.printStackTrace();
        }
    }

    public void refresh() {
        try {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "Nie mamy uprawnień do pobierania LastKnownLocation", Toast.LENGTH_LONG);
                return; //ten if sprawdza pozolenia odnoścnie uzyskiwania ostatniej lokacji, jak nie przechodzi to się wyjebie
            }
            String provider = locationManager.getBestProvider(criteria, true);
            location = locationManager.getLastKnownLocation(provider);
            /*location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if(location == null) {
                location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                if(location == null) {
                    location = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
                }
            }*/
        }
        catch (SecurityException e) {
            Toast.makeText(this, "Tu się jeszcze coś innego zjebało", Toast.LENGTH_LONG);
            e.printStackTrace();
        }
    }

    public void writeLatLong(){
        if(location!=null){
            heightText.setText("Długość: " + location.getLatitude());
            widthText.setText("Szerokość: " + location.getLongitude());
        }
        else{
            Toast.makeText(this, "Nie mamy dostępu do lokalizacji", Toast.LENGTH_LONG);
        }
    }

    @Override
    public void onLocationChanged(Location l) {
    this.location = l;
        writeLatLong();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {
        Toast.makeText(this, "Nie mamy providera", Toast.LENGTH_LONG);
    }
}
