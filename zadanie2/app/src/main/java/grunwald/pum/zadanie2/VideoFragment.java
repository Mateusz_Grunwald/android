package grunwald.pum.zadanie2;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.VideoView;


public class VideoFragment extends Fragment implements View.OnClickListener{

    Context context;
    View mView;
    public VideoView videoView;
    public Button button;
    public MediaController controller;
    public static int REQUEST_VIDEO_CAPTURE = 122; //kod do wywołania video

    public VideoFragment() {
    }

    public static VideoFragment newInstance() {
        VideoFragment fragment = new VideoFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_video, container, false); //tworzy view na podtsawie xml'a fragment_button
        button = mView.findViewById(R.id.videoButton);
        videoView =  mView.findViewById(R.id.videoView);

        button.setOnClickListener(this);
        context = getActivity().getApplicationContext();

        videoView.setOnTouchListener(new View.OnTouchListener() { //to tak ma być?
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return false;
            }
        });

        return mView;
    }

    @Override
    public void onClick(View v) {
        Intent takeVideo = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        startActivityForResult(takeVideo, REQUEST_VIDEO_CAPTURE); //odpal jeśli nasza intencja jest nasyzm kodem
    }

    //medota która wywali nam wynik aktywności
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_VIDEO_CAPTURE && resultCode == Activity.RESULT_OK) { //sprawdza czy nasze kody są spoko
            Uri videoUri = data.getData(); //wyciągamy uri
            if(controller == null) controller = new MediaController(getActivity()); //jeśli controller jest nullem to tworzymy sobie nowy
            videoView.setMediaController(controller);
            videoView.setVideoURI(videoUri);
            videoView.requestFocus();
            videoView.start();
        }
    }
}
