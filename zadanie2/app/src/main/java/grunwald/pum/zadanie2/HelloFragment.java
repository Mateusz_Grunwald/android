package grunwald.pum.zadanie2;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

//Wyrżnięty do oporu, bo to defacto takie okienko powitania więc chuja w tym potrzeba

public class HelloFragment extends Fragment {


    public HelloFragment() {
        // Required empty public constructor
    }

    public static HelloFragment newInstance() {
        HelloFragment fragment = new HelloFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_hello, container, false);
    }
}
