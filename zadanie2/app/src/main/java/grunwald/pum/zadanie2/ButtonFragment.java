package grunwald.pum.zadanie2;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


public class ButtonFragment extends Fragment {

    public Button bPhoto;
    public Button bMusic;
    public Button bVideo;

    public View mView;
    private OverviewFragmentActivityListener listener;


    public ButtonFragment() {
    }

    public static ButtonFragment newInstance() {
        ButtonFragment fragment = new ButtonFragment();
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    interface OverviewFragmentActivityListener { //interfejs do komunikacji z Main Activity
        public void onClickPhoto();
        public void onClickVideo();
        public void onClickMusic();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_button, container, false); //tworzy view na podtsawie xml'a fragment_button

        //przypisujemy przyciski do zmiennych klasy
        bPhoto = mView.findViewById(R.id.photo);
        bMusic = mView.findViewById(R.id.music);
        bVideo = mView.findViewById(R.id.video);

        //listenerowi przypisujemy zrzutowany context
        listener = (OverviewFragmentActivityListener) inflater.getContext();

        //customowy słuchacz do przycisków (wszystkich 3 na raz)
        onClickListener buttonListener = new onClickListener();
        bPhoto.setOnClickListener(buttonListener);
        bMusic.setOnClickListener(buttonListener);
        bVideo.setOnClickListener(buttonListener);

        return mView;
    }


    class onClickListener implements View.OnClickListener { //klasa tego cutomowego słuchacza

        @Override
        public void onClick(View v) {

            //sprawdzamy co klikneliśmy i w zależności od tego wywołujemy odpowiedni interface
            int id = v.getId();
            if (id == R.id.photo) listener.onClickPhoto();
            else if (id == R.id.music) listener.onClickMusic();
            else if (id == R.id.video) listener.onClickVideo();
            else System.out.println("something fucked up, dunno");
        }
    }
}
