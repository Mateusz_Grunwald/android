package grunwald.pum.zadanie2;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.icu.text.SimpleDateFormat;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;

import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;


public class PhotoFragment extends Fragment implements View.OnClickListener{
    private static final String TAG ="" ;
    public Context context;
    public View mView;
    public ImageView imageView;
    public Button button;
    public String currentPhotoPath;
    public static int REQUEST_CODE = 123; //kod do wywołania aparatu

    public Intent takePhoto; //zmienna intencji, czyli co chce zrobić program
    public File filePhoto;
    public Uri mUri; //yyy... tak. Zapewnia dostęp do ustrukturyzowanych danych i robi coś tam z uprawnieniami.


    public PhotoFragment() {

    }

    public static PhotoFragment newInstance() {
        PhotoFragment fragment = new PhotoFragment();
        return fragment;
    }
    //===============================
    //część sprawdzająca uprawnienia
    //===============================
    private boolean checkPermission(String[] permissions) { //sprawdza czy mamy androida powyżej marshmallow
        if(permissions != null && context != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            for(String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission)
                        != PackageManager.PERMISSION_GRANTED)
                    return false;
            }
        }
        return true;
    }

    private void requestPermission(String[] permissions) { // prosi o nadanie uprawnien:
        ActivityCompat.requestPermissions(getActivity(), permissions, 112);
    }

    //===============================
    //koniec części od uprawnień
    //===============================

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_photo, container, false); //tworzy view na podtsawie xml'a fragment_button
        button = mView.findViewById(R.id.photoButton);
        imageView =  mView.findViewById(R.id.photoView);

        button.setOnClickListener(this);
        context = getActivity().getApplicationContext();

        //jeszcze parę pierdół odnośnie uprawnień
        String[] perm = new String[] {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}; //sprawdza czy mamy uprawnienia do używania aparatu, jak nie to o nie prosi używając naszej funkcji
        if(!checkPermission(perm)) {
            requestPermission(perm);
        }
        return mView;
    }

    //nadawanie ścieżki do zdjęcia i unikalnej nazwy zdjęcia (trochę magii opd pawła)
    @RequiresApi(api = Build.VERSION_CODES.N)
    private File createImageFile() throws IOException {
        File image;
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "Multimedia_" + timeStamp;
        String[] perm = new String[] {Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE};
        if(!checkPermission(perm)) {
            Log.e(TAG, "Potrzebne uprawnienia zapisu i odczytu...");
            requestPermission(perm);
            return null;
        }
        File dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        image = File.createTempFile(imageFileName, ".jpg", dir);
        String mCurrentPhotoPath = image.getAbsolutePath();
        Log.d(TAG, "Current Photo Path: " + mCurrentPhotoPath);
        return image;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onClick(View v) {
        //robimy plik zdjęciowy
        try {
            filePhoto = createImageFile();
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        //tworzymy intencję aparatu
        takePhoto = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        //sprawdzamy czy plik zdjęciowy istnieje (czyli czy się stworzył) i jeśli tak to wyciągami jego Uri
        if(filePhoto!=null){
            mUri = FileProvider.getUriForFile(context, "com.example.multimedia", filePhoto);

            //wysyłąmy uri do intencji takePhoto (czyli mówimy gdzie ma zapisać zdjęcie)
            takePhoto.putExtra(MediaStore.EXTRA_OUTPUT,mUri);
            Log.d(TAG, "Uri to string: " + mUri.toString());

            //odpalamy aktywność aparatu
            startActivityForResult(takePhoto,REQUEST_CODE);
        }
        else {
            Log.d(TAG, "Nie mamy filePhoto!");
            Toast.makeText(context, "Nelezy zezwolic aplikacji na zapisywanie!", Toast.LENGTH_LONG).show();
        }
    }

    //medota która wywali mam wynik aktywności
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_CODE && resultCode == Activity.RESULT_OK) { //sprawdza czy nasze kody są spoko
            Bitmap image;
            try{
                image = MediaStore.Images.Media.getBitmap(context.getContentResolver(), mUri); //wyciągamy bitmapę z uri
                imageView.setImageBitmap(image); //wstawiamy bitmapę do naszego imageView
            } catch (FileNotFoundException e) {
                Log.d(TAG, "File not found, fuck off");
                e.printStackTrace();
            } catch (IOException e) {
                Log.d(TAG, "IO exception");
                e.printStackTrace();
            }
        }
    }
}
