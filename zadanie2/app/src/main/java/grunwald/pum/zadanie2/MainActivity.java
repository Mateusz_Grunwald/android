package grunwald.pum.zadanie2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import android.os.Bundle;

//dokładamy implementację tego naszego interfejsu
public class MainActivity extends AppCompatActivity implements ButtonFragment.OverviewFragmentActivityListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //pobieramy instancję do zmiennych i wstawiamy je w odpowiednie miejsca layoutu
        ButtonFragment buttons = ButtonFragment.newInstance();
        HelloFragment hello = HelloFragment.newInstance();
        getSupportFragmentManager().beginTransaction().add(R.id.upLayout, hello).commit();
        getSupportFragmentManager().beginTransaction().add(R.id.downLayout, buttons).commit();
    }


    //te 3 metody po prostu podmieniają nam górny fragment na inny
    @Override
    public void onClickPhoto() {
        PhotoFragment photo = PhotoFragment.newInstance();
        getSupportFragmentManager().beginTransaction().replace(R.id.upLayout, photo).commit();
    }

    @Override
    public void onClickVideo() {
        VideoFragment video = VideoFragment.newInstance();
        getSupportFragmentManager().beginTransaction().replace(R.id.upLayout, video).commit();
    }

    @Override
    public void onClickMusic() {
        MusicFragment music = MusicFragment.newInstance();
        getSupportFragmentManager().beginTransaction().replace(R.id.upLayout, music).commit();
    }
}
