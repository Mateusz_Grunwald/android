package grunwald.pum.zadanie2;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


public class MusicFragment extends Fragment implements View.OnClickListener {

    private static final String TAG ="" ;
    public View mView;
    Button buttonNext;
    Button buttonPrev;
    Button buttonVolUp;
    Button buttonVolDown;
    Button buttonMute;
    Button buttonPlay;
    List<String> musicList; //lista na muzykę
    TextView textView;
    static int played; //nr utworu
    boolean muted = false;
    MediaPlayer mp;
    static float volume;

    public MusicFragment() {
    }


    public static MusicFragment newInstance() {
        MusicFragment fragment = new MusicFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public List<String> getAllAudioFromDevice(){
        List<String> tempAudioList = new ArrayList<>();  //kontener na muzykę
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC);
        String path = storageDir.getAbsolutePath();
        File tmpFile, tmpSubfile;

        for(String tmp : storageDir.list()){ //iterujemy sobie po plikach w naszym storageDir
            tmpFile = new File(path + "/" + tmp);

            if(tmpFile.isDirectory()) { //jeśli z jakichś powodów to jest folderem to po nim też iterujemy
                for(String tmp2 : tmpFile.list()){
                    tmpSubfile = new File(path + "/" + tmp + "/" + tmp2);
                    if(!tmpSubfile.isDirectory()) tempAudioList.add(path + "/" + tmp + "/" + tmp2); //dodajemy do listy utowrów
                }
            }
            else{
                tempAudioList.add(path + "/" + tmp); //dodajemy do listy utowrów
            }
        }
        return tempAudioList;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_music, container, false); //tworzy view na podtsawie xml'a fragment_button

        buttonNext = mView.findViewById(R.id.musicButtonNext);
        buttonNext.setOnClickListener(this);
        buttonPrev = mView.findViewById(R.id.musicButtonPrev);
        buttonPrev.setOnClickListener(this);
        buttonVolUp = mView.findViewById(R.id.musicButtonVolumeUp);
        buttonVolUp.setOnClickListener(this);
        buttonVolDown = mView.findViewById(R.id.musicButtonVolumeDown);
        buttonVolDown.setOnClickListener(this);
        buttonMute = mView.findViewById(R.id.musicButtonMute);
        buttonMute.setOnClickListener(this);
        buttonPlay = mView.findViewById(R.id.musicButtonPlay);
        buttonPlay.setOnClickListener(this);



        musicList = getAllAudioFromDevice(); //wyciągamy całą muzykę z urządzenia naszą customową funkcją
        if(musicList.size()==0) Log.e(TAG, "Nie ma muzyki do wczytania");
        else{
            played=0; //odtwarzamy pierwszy utwór
            volume = (float) 0.5;
            mp = MediaPlayer.create(getActivity(), Uri.fromFile(new File(musicList.get(0)))); //tworzymy nowy media player dla pierwszego pliku.. nie wiem po co ale ok
            mp.setVolume(volume, volume); //prawy,lewy (wow, to ma stereo!)
        }
        return mView;
    }



    @Override
    public void onClick(View v) {
        textView = v.findViewById(R.id.musicTextView);
        switch (v.getId()) {

            case R.id.musicButtonPlay:
                if(mp.isPlaying()) mp.pause();
                else mp.start();
                break;

            case R.id.musicButtonVolumeUp:
                if(volume<1) volume = (float) (volume + 0.1);
                mp.setVolume(volume,volume);
                break;

            case R.id.musicButtonVolumeDown:
                if(volume>0) volume = (float) (volume - 0.1);
                mp.setVolume(volume,volume);
                break;

            case R.id.musicButtonMute:
                if(!muted){
                    mp.setVolume(0,0);
                    muted = true;
                }
                else{
                    mp.setVolume(volume,volume);
                    muted = false;
                }
                break;

            case R.id.musicButtonNext:
                if(played+1 < musicList.size()) {
                    mp.stop();
                    played++;
                }
                else{
                    mp.stop();
                    played=0;
                }

                try {
                    mp.reset();
                    String tmp = musicList.get(played); //wyciągamy ścieżkę do kolejnego utworu
                    mp.setDataSource(tmp); //zmieniamy utwór
                    textView.setText(tmp);
                    mp.prepareAsync();
                    mp.setOnPreparedListener(new MediaPlayer.OnPreparedListener() { //odpalamy
                        public void onPrepared(MediaPlayer mp) {
                            mp.start();
                        }
                    });
                }
                catch (Exception e) {
                    e.printStackTrace();
                    Log.e(TAG, "Coś się zwaliło z odtwarzaniem klolejnego utworu");
                }
                break;

            case R.id.musicButtonPrev:
                if(played > 0) {
                    mp.stop();
                    played--;
                }
                else{
                    mp.stop();
                    played= musicList.size()-1;
                }

                try {
                    mp.reset();
                    String tmp = musicList.get(played); //wyciągamy ścieżkę do kolejnego utworu
                    mp.setDataSource(tmp); //zmieniamy utwór
                    textView.setText(tmp);
                    mp.prepareAsync();
                    mp.setOnPreparedListener(new MediaPlayer.OnPreparedListener() { //odpalamy
                        public void onPrepared(MediaPlayer mp) {
                            mp.start();
                        }
                    });
                }
                catch (Exception e) {
                    e.printStackTrace();
                    Log.e(TAG, "Coś się zwaliło z odtwarzaniem poprzedniego utworu");
                }
                break;
        }

    }
}
