package grunwald.pum.zadanie4;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SignupActivity extends AppCompatActivity {
    EditText passEdit;
    EditText passEdit2;
    EditText loginEdit;
    EditText nameEdit;
    ConstraintLayout mainLayout;
    Button button;

    class MyTextWatcher implements TextWatcher{ //bedzie sprawdzac zmiany w obiektach EditText

        public EditText editText;
        public int id;

        public MyTextWatcher(EditText eT, int Id){
            editText = eT;
            id = Id;
            editText.addTextChangedListener(this); //możemy tak zrobić bo to przeciez implementuje textWatch'a
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            String typed = s.toString();
            String password;
            if(id == R.id.registerPass){ //jeśli edytujemy textedit z hasłem
                if(typed.length()<8) editText.setTextColor(Color.RED); //jak za krótkie hasło to będzie podświetlone na czerwono
                else editText.setTextColor(Color.GREEN);
            }
            else if(id == R.id.registerPass){ //jeśli edytujemy textedit z powtórzonym hasłem
                password = ((EditText)findViewById(R.id.registerPass)).getText().toString(); //wyciągamy to co wpisaliśmy w texedit wyżej
                Boolean check = password.equals(typed);
                if(check==false) editText.setTextColor(Color.RED); //tutaj też odnosimy się do tego dittext, bo listener każdy z listewnerów jest przypisany do osobnego z nich
                else editText.setTextColor(Color.GREEN);
            }


        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        passEdit =findViewById(R.id.registerPass);
        passEdit2 =findViewById(R.id.registerPass2);
        loginEdit = findViewById(R.id.registerLogin);
        nameEdit = findViewById(R.id.registerName);

        MyTextWatcher listener = new MyTextWatcher(passEdit,R.id.registerPass);
        MyTextWatcher listener2 = new MyTextWatcher(passEdit2,R.id.registerPass2);

        final DbAdapter mDbAdapter = new DbAdapter(getApplicationContext()); //bo teraz ustawiamy mu tylko context
        mainLayout = findViewById(R.id.mainSignupLayout);
        mainLayout.setBackgroundColor(MainActivity.ustawienia.getInt("backgroundColor", Color.WHITE)); //ustawi się ten co jest zapisany, jak go nie znajdzie to defaultowo będzie białe

        button = findViewById(R.id.signupButton);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String password = passEdit.getText().toString();
                String repeatedPassword = passEdit2.getText().toString();
                String login = loginEdit.getText().toString();
                String name = nameEdit.getText().toString();

                Boolean isPass = false; //muszę to zainicjalizować, bo inaczej android ma jakiś durny problem
                Boolean isLength = false;
                Boolean isText = false;

                if(!password.isEmpty() && !repeatedPassword.isEmpty()){ //sprawdzamy poszczególne flagi
                    isPass = password.equals(repeatedPassword);
                    if(password.length()>=8) isLength=true;
                    if(!name.isEmpty() && !login.isEmpty()) isText=true;
                }

                if(isPass && isLength && isText){ //jak wszystko jest git...
                    if(mDbAdapter.saveUser(login, password, name) == -999) { //ale łapiemy jakiś exception...
                        Toast.makeText(SignupActivity.this, "Nie można dodac tego użytkownika", Toast.LENGTH_LONG).show();
                        return;
                    }
                    Toast.makeText(SignupActivity.this, "Dodano użytkownika", Toast.LENGTH_LONG).show(); //a jak nie to jest spoko
                    mDbAdapter.close();
                    finish();
                }
                else {
                    if(!isText ) Toast.makeText(SignupActivity.this, "Nie wypełniono poprawnie wszystkich pól!", Toast.LENGTH_LONG).show();
                    else if(!isLength) Toast.makeText(SignupActivity.this, "Hasło powinno mieć minimum 8 znaków!", Toast.LENGTH_LONG).show();
                    else if(!isPass) Toast.makeText(SignupActivity.this, "Hasła się nie zgadzaja!", Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}
