package grunwald.pum.zadanie4;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

public class SensorClass implements SensorEventListener {

    SensorActivity sensorActivity; //Aktywność w której uruchamiamy sensory
    Sensor sensor;
    SensorManager sensorManager;

    public SensorClass(SensorActivity SensorActivity, Sensor Sensor, SensorManager SensorManager){
        SensorActivity = sensorActivity;
        Sensor = sensor;
        SensorManager = sensorManager;
    }
    @Override
    public void onSensorChanged(SensorEvent event) {
        String tmp = "";
        int type = sensor.getType(); //bierzemy typ sensora
        if(type == Sensor.TYPE_MAGNETIC_FIELD) {
            tmp += event.values[0] + " " + event.values[1] + " " + event.values[2] + " \u03BCT"; //bierzemy 3 wartości z sensora i wpisujemy jednostę (\u03BCT to μT)
        }
        else if(type == Sensor.TYPE_ACCELEROMETER) {
            tmp += event.values[0] + " " + event.values[1] + " " + event.values[2] + " m/sˆ2"; //jak wyżej tylko inny sensor
        }
        else if(type == Sensor.TYPE_AMBIENT_TEMPERATURE) {
            tmp += event.values[0] + " \u00B0C"; // \u00B0C to °C
        }
        sensorActivity.setText(tmp);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    public void onResume() { //rejestrujemy sensor
        sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_UI);
    }
    public void onPause() { //wyrejestrowujemy
        sensorManager.unregisterListener(this);
    }
}
