package grunwald.pum.zadanie4;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity{

    static SharedPreferences ustawienia; //do modyfikacji preferencji tej jednej instancji która dzielą wszyscy klienci.
    RelativeLayout mainLayout;
    AlertDialog.Builder alertDialogBuilder;

    Button dialogButton;
    Button loginButton;
    EditText loginText;
    EditText passText;

    int color = Color.WHITE;

    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.main_menu, menu); //wyciągamy menu z xml'a
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item){ //odpowiedź na wybranie odpowiedniego elementu menu
        int id = item.getItemId(); // pobieramy id klikniętego elementu menu
        if(id==R.id.resetBkg){
            mainLayout.setBackgroundColor(Color.WHITE);
            SharedPreferences.Editor ed = ustawienia.edit();
            ed.putInt("backgroundColor", Color.WHITE);
            ed.commit();
        }
        else if(id==R.id.register){
            Intent intent = new Intent(getApplicationContext(), SignupActivity.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ustawienia = this.getSharedPreferences("mPreferences", 0);// tryb prywatny - tylko wywołująca apka ma do tego dostęp
        ustawienia.getInt("logged", 0); //sprawdzamy czy mamy w pliku zmienną "logged", jesli nie wywalamy 0

        mainLayout = findViewById(R.id.mainLayout);
        dialogButton = findViewById(R.id.dialogButton);
        loginButton =  findViewById(R.id.loginButton);
        loginText =  findViewById(R.id.login);
        passText = findViewById(R.id.password);

        //rzeczy od dialog buildera
        alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Wybierz kolor tła");
        final SharedPreferences.Editor edytor = ustawienia.edit(); //edytor do naszych ustawień

        //ustawia nam domyślny kolor tła
        edytor.putInt("backgroundColor", Color.WHITE);
        edytor.commit();
        if(!ustawienia.contains("backgroundColor")){
            edytor.putInt("backgroundColor", Color.WHITE);
            edytor.commit();
        }

        //listener, który umożliwi wybór koloru tła
        DialogInterface.OnClickListener dialogListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case Dialog.BUTTON_POSITIVE: //przycisk "niebieski"
                        if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) { //uwzględnienie wersji SDK
                            color = getResources().getColor(R.color.blue);
                        }
                        else {
                            color = getColor(R.color.blue);
                        }
                        break;

                    case Dialog.BUTTON_NEGATIVE:
                        if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
                            color = getResources().getColor(R.color.red);
                        }
                        else {
                            color = getColor(R.color.red);
                        }
                        break;
                    default: //defaultowo będzie białe
                        if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) { //uwzględnienie wersji SDK
                            color = getResources().getColor(R.color.white);
                        }
                        else {
                            color = getColor(R.color.white);
                        }
                        break;
                }
                mainLayout.setBackgroundColor(color);
                edytor.putInt("backgroundColor", color); //zapisze wybrany przez nas kolor w edytorze
                edytor.commit();
            }
        };
        alertDialogBuilder.setPositiveButton("Niebieski", dialogListener); //przyciski do ustawienia kolorów
        alertDialogBuilder.setNegativeButton("Czerwony", dialogListener);


        //oprogramowanie buttona od okienka dialogowego
        View.OnClickListener dialogButtonListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog alert = alertDialogBuilder.create();
                alert.show();
            }
        };
        dialogButton.setOnClickListener(dialogButtonListener);

        //Rzeczy do logowania, dopracować bo moze się wyjebać
        DbAdapter mAdapter = new DbAdapter(this);
        mAdapter.open();
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String veryfieUser = DbAdapter.checkUser(loginText.getText().toString(),passText.getText().toString());
                if(veryfieUser==null){
                    Toast.makeText(MainActivity.this, "Złe dane użytkownika!", Toast.LENGTH_LONG).show();
                }
                else{
                    String name = MainActivity.ustawienia.getString("name", "Uzytkowniku");
                    Toast.makeText(MainActivity.this, "Zalogowano...", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(MainActivity.this, SensorActivity.class);
                    intent.putExtra("LOGIN_NAME", name);
                    startActivity(intent);
                }

            }
        });







    }

}
