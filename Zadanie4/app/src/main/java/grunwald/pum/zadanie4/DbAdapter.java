package grunwald.pum.zadanie4;
//Klasa pomagająca przy obsłudze bazy danych

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.SQLException;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DbAdapter {

    public Context context;

    public static final String COL_ID = "_id";
    private static final String TABLE_NAME = "zad4table"; //te 2 wbiłem na sztywno, bo w sumie nie wiem po co mamy je ustawiać inaczej
    private static final String DATABASE_NAME = "zad4database";
    public String TABLE_CREATE;
    private static final int DATABASE_VERSION = 1;

    //kolumny
    public static final String COL_LOGIN = "login";
    public static final String COL_PASSWORD = "pass";
    public static final String COL_NAME = "name";

    /*public DbAdapter(Context c, String dbName, String tableName){
        this.context = c;
        DATABASE_NAME = dbName;
        TABLE_NAME = tableName;
    }*/

    public DbAdapter(Context c){
        this.context = c;
    }

    private DatabaseHelper dbHelper; //zmienna do helpera
    private static SQLiteDatabase myDb; //zmienna do bazy

    // klasa pomocnicza do bazy
    class DatabaseHelper extends SQLiteOpenHelper{

        public DatabaseHelper(Context context, String name, int version, String table) {
            super(context, name ,null, version);
            TABLE_CREATE = "CREATE TABLE if not exists " + TABLE_NAME +
                    "(" + COL_ID + " INTEGER PRIMARY KEY autoincrement, " +
                    COL_LOGIN + " TEXT UNIQUE, " +
                    COL_PASSWORD + " TEXT, " +
                    COL_NAME + " TEXT);";
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(TABLE_CREATE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
            onCreate(db); //robi nową
        }

    }


    //Funckcje do otwierania i zamykania bazy
    public void open() throws SQLException {
        dbHelper = new DatabaseHelper(context, DATABASE_NAME, DATABASE_VERSION,TABLE_NAME); //wersję wbijam na sztywno na 1 bo tak było w zadaniu 1 i to działało
        myDb = dbHelper.getWritableDatabase(); //zwraca obiekt bazy, do którego mozemy coś zapisać
    }
    public void close() {
        if(dbHelper != null) {
            dbHelper.close();
        }
    }

    //Użytkownicy
    public long saveUser(String login, String pass, String name){
        long id = 0; //id elementu w bazie
        ContentValues userData = new ContentValues(); //Taka turbotablicolista lub pseudobaza - podajemu jej w .put() nazwę czegoś a potem jego wartośc
        userData.put("login",login);
        userData.put("pass",pass);
        userData.put("name",name);
        try {
            id = myDb.insertOrThrow(TABLE_NAME, null, userData);
        }
        catch(SQLiteConstraintException exp) {
            return -999;
        }
        return id;
    }

    public static String checkUser(String login, String pass){ //sprawdza czy istnieje user z takim loginem i hasłem i jak tak to wyrzuci jego imie
        Cursor cursor = myDb.query(TABLE_NAME, new String[] {COL_NAME},
                COL_LOGIN + "=? AND " + COL_PASSWORD + "=?",
                new String[] {login, pass}, null, null, null);

        if(cursor!=null){
            cursor.moveToFirst();
        }
        else return null; //null jak brak użytkownika

        if(cursor.getCount()==0) return null; //null jak pusty record
        else return cursor.getString(0);
    }
}
