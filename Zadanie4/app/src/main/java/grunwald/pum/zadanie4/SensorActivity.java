package grunwald.pum.zadanie4;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class SensorActivity extends AppCompatActivity {

    ConstraintLayout mainLayout;
    TextView textScrollView;
    TextView textHelloView;
    Button magneticFieldButton;
    Button accelerationButton;
    Button temperatureButton;
    Button stopButon;
    Button logoutButton;
    int sensorRun = 0; //zmienna, która okresla nam czy Sensor jest uruchomiony
    SensorManager sensorManager;
    Sensor sensor;
    SensorClass sensorClass;
    SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sensor);

        //wyciągamy to wszystko z resouces
        mainLayout = findViewById(R.id.mainSensorLayout);
        textScrollView = findViewById(R.id.inScrollText);
        textHelloView = findViewById(R.id.sensorHelloText);
        magneticFieldButton = findViewById(R.id.MagneticFieldButton);
        accelerationButton = findViewById(R.id.AccelerationButton);
        temperatureButton = findViewById(R.id.TemperatureButton);
        stopButon = findViewById(R.id.StopButton);
        logoutButton = findViewById(R.id.LogoutButton);
        preferences = MainActivity.ustawienia;

        mainLayout.setBackgroundColor(preferences.getInt("backgroundColor", Color.WHITE));
        String name  = getIntent().getStringExtra("LOGIN_NAME"); //wyciągamy imie z intentu signup'a
        textHelloView.setText("cześć " + name +", co dzisiaj sprawdzamy?");
        sensor=null;
        sensorManager = (SensorManager)getSystemService(Context.SENSOR_SERVICE); //musimy znaleźć sensor manager, rzutujemy na odpowiedni typ

        View.OnClickListener listener = new View.OnClickListener() { //słuchacz do przycisków
            @Override
            public void onClick(View v) {
                int id = v.getId(); //bierzemy id tego co klikamy

                if(id == R.id.MagneticFieldButton) {
                    textScrollView.setText("");
                    sensor = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD); //ustawiamy nasz sensor na sensor magnetyczny
                    if(sensor != null) {
                        if(sensorRun == 0) {
                            sensorRun = 1;
                            sensorClass = new SensorClass(SensorActivity.this, sensor , sensorManager);
                            sensorClass.onResume();
                        }
                        else {
                            Toast.makeText(SensorActivity.this, "Powoli, nie odpalaj 2 razy tego samego.", Toast.LENGTH_LONG).show();
                        }
                    }
                    else {
                        Toast.makeText(SensorActivity.this, "Nie mamy sensora magnetycznego, sory typie, kup lepszy telefon czy coś...", Toast.LENGTH_LONG).show();
                    }
                }

                else if(id == R.id.AccelerationButton) {
                    textScrollView.setText("");
                    sensor = sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
                    if(sensor != null) {
                        if(sensorRun == 0) {
                            sensorRun = 1;
                            sensorClass = new SensorClass(SensorActivity.this, sensor , sensorManager);
                            sensorClass.onResume();
                        }
                        else {
                            Toast.makeText(SensorActivity.this, "Powoli, nie odpalaj 2 razy tego samego.", Toast.LENGTH_LONG).show();
                        }
                    }
                    else {
                        Toast.makeText(SensorActivity.this, "Nie mamy akceleratora, sory typie, kup lepszy telefon czy coś...", Toast.LENGTH_LONG).show();
                    }
                }

                else if(id == R.id.TemperatureButton) {
                    textScrollView.setText("");
                    sensor = sensorManager.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE);
                    if(sensor != null) {
                        if(sensorRun == 0) {
                            sensorRun = 1;
                            sensorClass = new SensorClass(SensorActivity.this, sensor , sensorManager);
                            sensorClass.onResume();
                        }
                        else {
                            Toast.makeText(SensorActivity.this, "Powoli, nie odpalaj 2 razy tego samego.", Toast.LENGTH_LONG).show();
                        }
                    }
                    else {
                        Toast.makeText(SensorActivity.this, "Nie mamy termometra, sory typie, kup lepszy telefon czy coś...", Toast.LENGTH_LONG).show();
                    }
                }

                else if(id == R.id.StopButton) {
                    if(sensorRun == 1) {
                        sensorClass.onPause();
                        sensorRun = 0;
                    }
                    else {
                        Toast.makeText(SensorActivity.this, "Żaden sensor nie działą, po cholere klikasz?", Toast.LENGTH_LONG).show();
                    }
                }

                else if (id == R.id.LogoutButton) {
                    SharedPreferences.Editor edytor = preferences.edit(); //otwieramy nasz edytor do preferencji
                    edytor.putString("name", "");
                    edytor.putInt("logged", 0); //wylogowujemy typa
                    edytor.commit(); //commitujemy
                    finish();
                }

            }
        };

        magneticFieldButton.setOnClickListener(listener);
        accelerationButton.setOnClickListener(listener);
        temperatureButton.setOnClickListener(listener);
        stopButon.setOnClickListener(listener);
        logoutButton.setOnClickListener(listener);

    }

    public void setText(String txt) {
        textScrollView.append("\n"+txt);
    }
}
