package grunwald.pum.zadanie1;

import androidx.appcompat.app.AppCompatActivity;

import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    //nasze zmienne do obiektów z .xml'a
    private ListView listView;
    private EditText editText;
    private Button button;
    private Button cButton;
    private CheckBox checkBox;
    private ArrayAdapter<String> adapter;
    private ListDbAdapter dbAdapter;
    private EntrySimpleCursorAdapter cursorAdapter;
    private static final String TAG = "MainActivity";


    @Override
    protected void onCreate(Bundle savedInstanceState) { //konstruktor tworzący rzeczy na początku apki
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = findViewById(R.id.list); //przypisujemy nasze elementy do zmiennych przez id
        editText = findViewById(R.id.textEdit);
        button = findViewById(R.id.button);
        cButton = findViewById(R.id.clearButton);
        //checkBox = findViewById(R.id.listCheckbox);

        dbAdapter = new ListDbAdapter(this);
        dbAdapter.open();
        Cursor cursor = dbAdapter.fetchAllEntries(); //zasysamy wszystkie pliki z bazy

        //tworzymy adapter na rzeczy z bazy
        String[] from = new String[] {ListDbAdapter.COL_CONTENT};
        int[] to = new int[] {R.id.textview};

        cursorAdapter = new EntrySimpleCursorAdapter(this, R.layout.list_style, cursor, from, to, 0); //adapter, który łączy listę z bazą

        //adapter = new ArrayAdapter<>(this, R.layout.list_style, R.id.textview); //zwykły, bez uwzględnienia bazy danych
        listView.setAdapter(cursorAdapter);
        //listView.setAdapter(adapter);

        button.setOnClickListener( //przycisk do dodawania
                new View.OnClickListener(){
                    @Override
                    public void onClick(View v) {
                        String item = editText.getText().toString();
                        dbAdapter.createList(item, false);
                        Cursor c = dbAdapter.fetchAllEntries();
                        cursorAdapter.changeCursor(c);
                        editText.setText("");

                    }
                }
        );

        // TODO - listView -> dodać setOnClick listener
        // TODO - trzeba wydobyć ID, ale one się nie kasuja tylko dopisują nowe, więc on click da nam pozycję a nie id.
        // TODO - bierzemy holder danego elementu (patrz Viewholder) i wyciągamy z niego id
        // TODO - okienko dialogowe ustawiamy też na liście (listView)

        listView.setOnItemClickListener( //będzie sprawdzać checkboxy
                new AdapterView.OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) { //tutaj zassaliśmy całą listę i teraz możemy patrzeć na pojedyńcze elementy
                        CheckBox cb = ((CheckBox)((View) view.findViewById(R.id.listCheckbox))); // znajdujemy checkbox w liście
                        boolean isChecked = cb.isChecked();

                        if(!isChecked){ //jeśli nie jest checknięty to teraz zrób aby był (bo w niego kliknelismy)
                            isChecked = true;
                            cb.setChecked(isChecked);
                        }
                        else{ //a jak był to go teraz odklikujemy
                            isChecked = false;
                            cb.setChecked(isChecked);
                        }


                        EntrySimpleCursorAdapter.ViewHolder2 holder = (EntrySimpleCursorAdapter.ViewHolder2) view.getTag(); //Tutaj mam kopię ViewHoldera wyciągniętego na zewnątrz, bo inaczej się nie importuje bo tak.
                        if(holder != null){ //jeśli mamy złapane jakieś entry
                            String text = ((TextView) view.findViewById(R.id.textview)).getText().toString();
                            dbAdapter.updateEntry(new ListDbAdapter.Entry(holder.id, text, isChecked)); //updatujemy nasze entry

                            Log.d(TAG, "Kliknięto " + position + " element listy, zaznaczony: " + isChecked);
                            Log.d(TAG, "dbId: " + holder.id);
                        }
                    }
                }
        );


        cButton.setOnClickListener( //przycisk do usuwania
                new View.OnClickListener(){
                    @Override
                    public void onClick(View v) {
                       //nie wiem :(

                    }
                }
        );


    }
    //funkcja do wywołania menu
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    //funkcja do ogarnięcia przycisku
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId(); // pobieramy id klikniętego elementu menu
        if(id == R.id.menuItem1) { // R.id.nazwa_pobiera - daje dostęp do id konkretnego elementu

            Toast.makeText(getApplicationContext(), getString(R.string.menu_item), Toast.LENGTH_LONG).show(); // Toast wyświetla informację okienku
            return true;
        }
        if(id ==R.id.menuItem2){
            //adapter.clear();
            //adapter.notifyDataSetChanged()
            dbAdapter.deleteAllEntries();
            Cursor c = dbAdapter.fetchAllEntries();
            cursorAdapter.changeCursor(c);
        }
        return super.onOptionsItemSelected(item);
    }

}
