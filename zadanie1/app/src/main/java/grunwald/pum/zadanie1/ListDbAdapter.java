package grunwald.pum.zadanie1;
//Klasa pomagająca przy obsłudze bazy danych

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import androidx.annotation.Nullable;

public class ListDbAdapter {

    // region inits (dbAdapter, dbHelper)

    Context context; //ta dziwna zmienna, która jest wywoływana na toolsach w xml'u, To chyba jakiś gate na MainActivity czy coś...
    //NAZWY KOLUMN
    public static final String COL_ID = "_id";
    public static final String COL_CONTENT = "content";
    public static final String COL_CHECKED = "checked";

    //INDEKS KOLUMN
    public static final int INDEX_ID = 0;
    public static final int INDEX_CONTENT = INDEX_ID + 1;
    public static final int INDEX_CHECKED = INDEX_CONTENT + 1;

    private static final String TAG = "ListDbAdapter"; // będzie wykorzystywany w logach
    private static final String DATABASE_NAME = "dba_shopping";
    private static final int DATABASE_VERSION = 1;
    private static final String TABLE_NAME = "tbl_shopping";

    private static final String DATABASE_CREATE = "CREATE TABLE if not exists " //zapytanie SQLa, tworzące tabelę o 3ch kolumnach, określonych przez zmienne COL_*
            + TABLE_NAME + " ( "
            + COL_ID + " INTEGER PRIMARY KEY autoincrement, "
            + COL_CONTENT + " TEXT, "
            + COL_CHECKED + " INTEGER DEFAULT 0);";

    public ListDbAdapter(Context c){
        this.context = c;
    }

    private DatabaseHelper dbHelper; //zmienna do helpera
    private SQLiteDatabase myDb; //zmienna do bazy

    // klasa pomocnicza do bazy
    class DatabaseHelper extends SQLiteOpenHelper{

        public DatabaseHelper(Context context) { //tylko tom bo reszta idzie na twardo
            super(context, DATABASE_NAME,null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(DATABASE_CREATE); //tworzy nam naszą tabelę
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME); //usuwa nam tabelę (normalnie powinno robić migrację, ale ni cholery nie wiem jak to się robi)
            onCreate(db); //robi nową
        }

    }

    //endregion

    //klasa do obsługi wierszy
    static class Entry{
        private int id;
        private String text;
        private boolean isChecked;

        public Entry(){
            this.id = 0;
            this.text = null;
            this.isChecked = false;
        }

        public Entry(int ID, String TXT, boolean CHCK){
            this.text = TXT;
            this.isChecked = CHCK;
            this.id = ID;
        }

        //settery
        public void setID(int ID){
            this.id = ID;
        }
        public void setText(String TXT){
            this.text = TXT;
        }
        public void setChecked(boolean CHCK){
            this.isChecked = CHCK;
        }


        //gettery
        public int getID(){
            return this.id;
        }
        public String getText(){
            return this.text;
        }
        public boolean isChecked(){
            return  this.isChecked;
        }
    }

    //Funckcje do otwierania i zamykania bazy
    public void open() throws SQLException {
        dbHelper = new DatabaseHelper(context);
        myDb = dbHelper.getWritableDatabase(); //zwraca obiekt bazy, do którego mozemy coś zapisać
    }
    public void close() {
        if(dbHelper != null) {
            dbHelper.close(); //close jest pewnie z SQLiteOpenHelper
        }
    }

    // region zapisy do bazy
    //zapis do bazy (z obektem entry)
    public long createEntry(Entry entry){
        Log.w(TAG, "Create entry with id: "+entry.getID());
        ContentValues values = new ContentValues();
        values.put(COL_CONTENT, entry.getText());
        values.put(COL_CHECKED, entry.isChecked());
        return myDb.insert(TABLE_NAME, null, values); //zwraca index wiersza
    }

    //zapis do bazy ze składowymi
    public long createList(String text, boolean isChecked){
        ContentValues values = new ContentValues();
        values.put(COL_CONTENT, text);
        values.put(COL_CHECKED, isChecked);
        return myDb.insert(TABLE_NAME, null, values); //zwraca index wiersza
    }

    //aktualizacja wiersza
    public void updateEntry(Entry entry) {
        Log.w(TAG, "Update entry with id: "+entry.getID());
        ContentValues values = new ContentValues();
        values.put(COL_CONTENT, entry.getText());
        values.put(COL_CHECKED, entry.isChecked());
        myDb.update(TABLE_NAME, values,
                COL_ID + "=?", new String[] {String.valueOf(entry.getID())});
    }

    //pobranie ilości recordów z bazy
    public long howManyEntries(){
        return DatabaseUtils.queryNumEntries(myDb, TABLE_NAME);
    }

    //endregion do bazy

    //region zwracanie wierszy

    //zwraca jeden wiersz z bazy (po ID)
    public Entry fetchEntryById(int id) {
        int Id = 0;
        String content = null;
        boolean checked = false;
        Log.w(TAG, "Fetch entry with id: "+id);
        Cursor cursor = myDb.rawQuery("SELECT * FROM " + TABLE_NAME+ " WHERE " + COL_ID + " = " +id, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                Id = cursor.getInt(INDEX_ID);
                content = cursor.getString(INDEX_CONTENT);
                checked = cursor.getInt(INDEX_CHECKED) == 1 ? true : false;
            }
        }
        return new Entry(Id,content,checked);
    }

    //zwraca wszystkie wiersze z bazy
    public Cursor fetchAllEntries() {
        Cursor cursor = myDb.query(TABLE_NAME, new String[] {COL_ID, COL_CONTENT, COL_CHECKED},
                null,
               null, null, null, null); //bo tu nie potrzebujemy żadnych warunków
        if(cursor != null) cursor.moveToFirst();
        return cursor;
    }
    //endregion

    // region usuwanie wierszy
    //usuwanie wiersza
    public void deleteEntry(int id) {
        Log.w(TAG, "Delete entry with id: "+id);
        myDb.delete(TABLE_NAME, COL_ID + "=?", new String[] {String.valueOf(id)});
    }

    //usuwanie wszystkich wierszy
    public void deleteAllEntries() {
        Log.w(TAG, "All entries delete");
        myDb.delete(TABLE_NAME, null, null);
    }

    //endregion





}
