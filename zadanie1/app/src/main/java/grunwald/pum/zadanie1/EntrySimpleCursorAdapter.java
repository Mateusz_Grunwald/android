package grunwald.pum.zadanie1;

import android.content.Context;
import android.database.Cursor; //taha karetka jak przy pisaniu, wskazuje na rząd danych, taki ala wskaźnik
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;


//klasa zastępująca nam ArrayAdapter (dodanie wczytanych danych do listy)
public class EntrySimpleCursorAdapter extends SimpleCursorAdapter {
    public EntrySimpleCursorAdapter(Context context, int layout, Cursor c, String[] from, int[] to, int flags) {
        super(context, layout, c, from, to,flags);
    }

    //przeciążone metody z klasy nadrzędnej
    @Override
    public View newView(Context c, Cursor cu, ViewGroup parent) {
        return super.newView(c,cu,parent);
    }

    public void bindView(View view, Context context, Cursor cursor){
        super.bindView(view, context, cursor);

        //Wewnętrzna klasa, która ogarniam nam połączenie Entry z View
        class ViewHolder{
            int id;
            boolean check;
            String text;
            View listTab;
        }

        ViewHolder holder = (ViewHolder) view.getTag(); //bierzemy sobie View z OnBind (ten, który powinien znajdowac się tam wcześniej, tzn jakiś layout czy item.
        if(holder == null) { //jeśli nie ma żądnego wczesniej to tworzymy własny
            holder = new ViewHolder();
            holder.id = cursor.getInt(ListDbAdapter.INDEX_ID);
            holder.text = cursor.getString(ListDbAdapter.INDEX_CONTENT);
            holder.check = cursor.getInt(ListDbAdapter.INDEX_CHECKED) == 1 ? true : false;
            holder.listTab = view.findViewById(R.id.textview); //ODNOSNIK R.id. DO ELEMENTU LISTY! NIE TEXT EDITA, TYLKO DO KUR... LISTY!
            view.setTag(holder);
        }

        //ustalamy sobie style wyglądu (różny dla wierszy parzystych i nieparzystych)
        int cursorPositionID = cursor.getPosition();
        if(cursorPositionID%2==0){
            ((View)holder.listTab.getParent()).setBackgroundColor(context.getResources().getColor(R.color.darkBackgroundColor));
            ((TextView)holder.listTab.findViewById(R.id.textview)).setTextColor(context.getResources().getColor(R.color.lightTextColor));
        }
        else{
            ((View)holder.listTab.getParent()).setBackgroundColor(context.getResources().getColor(R.color.lightBackgroundColor));
            ((TextView)holder.listTab.findViewById(R.id.textview)).setTextColor(context.getResources().getColor(R.color.darkTextColor));
        }
    }

    public class ViewHolder2{ //kopia holdera, bo tamten się nie chce importować
        int id;
        boolean check;
        String text;
        View listTab;
    }
}


