package grunwald.pum.anomalydetector;

import android.graphics.Color;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;


public class Anomaly {

    double latitude;
    double longitude;
    double radius;
    double radiationLvl;
    double electricLvl;
    double magneticLvl;
    int isSelected;
    long baseID;
    CircleOptions range;
    Circle drawnRange;

    public Anomaly(double lat, double lon, double radius, double radiation, double electric, double magnetic, long dBID){
        this.latitude = lat;
        this. longitude = lon;
        this.radius = radius;
        this.radiationLvl = radiation;
        this.electricLvl = electric;
        this.magneticLvl = magnetic;
        this.baseID = dBID;
        this.isSelected = 0;

        this.range = new CircleOptions();
        this.range.center(new LatLng(lat, lon));
        this.drawnRange = null;
    }

    public void changeParameters(double radius, double radiation, double electric, double magnetic){
        this.radius = radius;
        this.radiationLvl = radiation;
        this.electricLvl = electric;
        this.magneticLvl = magnetic;
    }

    public void drawRange(GoogleMap map){
        //kolory wklepane manualnie, bo linkowanie z colors.xml z jakiegoś powodu tu nie działa. Do podejrzenia w xml'u
        this.range.radius(this.radius);
        this.range.strokeColor(0xFFC70000);
        this.range.fillColor(0x40EF0000);
        this.range.strokeWidth(3);
        this.drawnRange = map.addCircle(this.range);
    }

    public void toggleActive(GoogleMap map){
        if(this.isSelected==0){
            this.drawnRange.remove();
            this.range.strokeColor(0xFF00C700);
            this.range.fillColor(0x4000EF00);
            this.range.strokeWidth(3);
            this.drawnRange = map.addCircle(this.range);
            this.isSelected = 1;
        }
        else if(this.isSelected==1){
            this.drawnRange.remove();
            this.range.strokeColor(0xFFC70000);
            this.range.fillColor(0x40EF0000);
            this.range.strokeWidth(3);
            this.drawnRange = map.addCircle(this.range);
            this.isSelected = 0;
        }
    }
    public double measurementValue(double distance, double value){
        return value * Math.exp(-5*Math.pow(distance/this.radius,2));
    }
}
