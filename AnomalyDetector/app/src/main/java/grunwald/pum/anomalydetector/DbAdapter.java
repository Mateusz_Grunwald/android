package grunwald.pum.anomalydetector;
//Klasa pomagająca przy obsłudze bazy danych

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DbAdapter {

    public Context context;

    private static final String TAG = "DbAdapter";

    public static final String COL_ID = "id";
    private static final String TABLE_NAME = "anomaliesList";
    private static final String DATABASE_NAME = "detectorDataBase";
    public String TABLE_CREATE;
    private static final int DATABASE_VERSION = 8;

    //kolumny
    public static final String COL_LAT = "latitude";
    public static final String COL_LON = "longitude";
    public static final String COL_RADIUS = "radius";
    public static final String COL_RADIATION = "radiationLvl";
    public static final String COL_ELECTRIC = "electricLvl";
    public static final String COL_MAGNETIC = "magneticLvl";


    public DbAdapter(Context c){
        this.context = c;
    }

    private DatabaseHelper dbHelper; //zmienna do helpera
    private static SQLiteDatabase myDb; //zmienna do bazy

    // klasa pomocnicza do bazy
    class DatabaseHelper extends SQLiteOpenHelper{

        public DatabaseHelper(Context context, String name, int version, String table) {
            super(context, name ,null, version);
            TABLE_CREATE = "CREATE TABLE if not exists " + TABLE_NAME +
                    "(" + COL_ID + " INTEGER PRIMARY KEY autoincrement, " +
                    COL_LAT + " REAL, " +
                    COL_LON + " REAL, " +
                    COL_RADIUS + " REAL, " +
                    COL_RADIATION + " REAL, " +
                    COL_ELECTRIC + " REAL, " +
                    COL_MAGNETIC + " REAL)";
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(TABLE_CREATE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
            onCreate(db); //robi nową
        }
    }

    public void open() throws SQLException {
        dbHelper = new DatabaseHelper(context, DATABASE_NAME, DATABASE_VERSION,TABLE_NAME);
        myDb = dbHelper.getWritableDatabase(); //zwraca obiekt bazy, do którego mozemy coś zapisać
    }
    public void close() {
        if(dbHelper != null) {
            dbHelper.close();
        }
    }

    //anomalie
    public long saveAnomaly(double lat, double lon, double radius, double radiation, double electric, double magnetic){
        long baseId = 0;
        ContentValues anomaliesData = new ContentValues(); //Taka turbotablicolista lub pseudobaza - podajemu jej w .put() nazwę czegoś a potem jego wartośc
        anomaliesData.put("latitude",lat);
        anomaliesData.put("longitude",lon);
        anomaliesData.put("radius",radius);
        anomaliesData.put("radiationLvl",radiation);
        anomaliesData.put("electricLvl",electric);
        anomaliesData.put("magneticLvl",magnetic);
        try {
            baseId = myDb.insertOrThrow(TABLE_NAME, null, anomaliesData);
            Log.w(TAG, "new anomaly with base id: "+baseId);
        }
        catch(SQLiteConstraintException exp) {
            Log.e("saveAnomaly", "exception", exp);
            return -999;
        }
        return baseId;
    }
    public void updateAnomaly(long baseID, double radius, double radiation, double electric, double magnetic){
        long status = 0;
        ContentValues anomaliesData = new ContentValues();
        anomaliesData.put("radius",radius);
        anomaliesData.put("radiationLvl",radiation);
        anomaliesData.put("electricLvl",electric);
        anomaliesData.put("magneticLvl",magnetic);
        try {
            status = myDb.update(TABLE_NAME, anomaliesData,COL_ID+" =?", new String[]{String.valueOf(baseID)});
            Log.w(TAG, "updated anomaly with status: "+status);
        }
        catch(SQLiteConstraintException exp) {
            Log.e("updateAnomaly", "exception", exp);
        }
    }
    public void deleteAnomaly(long baseID) {
        long status = 0;
        try{
            status = myDb.delete(TABLE_NAME,COL_ID+" =?",new String[]{String.valueOf(baseID)});
            Log.w(TAG, "deleted anomaly with status: "+status);
        }
        catch (SQLiteConstraintException exp){
            Log.e("deleteAnomaly", "exception", exp);
        }
    }

    public Cursor fetchAllActiveAnomalies() {
        Cursor cursor = myDb.rawQuery("SELECT * FROM " + TABLE_NAME, null);
        if(cursor != null) cursor.moveToFirst();
        return cursor;
    }
}
