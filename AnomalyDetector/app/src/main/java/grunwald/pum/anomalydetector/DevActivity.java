package grunwald.pum.anomalydetector;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Typeface;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

public class DevActivity extends FragmentActivity implements OnMapReadyCallback, LocationListener{

    private GoogleMap mMap;
    LocationManager locationManager; //zapewnia dostęp do usługi lokalizacji
    Criteria criteria; //wskazuje na kryteria aplikacji do wyboru dostawcy lokalizacji
    Location location; //położenie geograficzne
    Marker locationMarker; //marker położenia
    String[] perm = new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}; //lista uprawnień

    DbAdapter mDbAdapter;
    private static final String TAG = "DevMode";

    double latitude;
    double longitude;
    float zoomLevel = 20.0f;
    int INPUT_TYPE = InputType.TYPE_CLASS_NUMBER |  InputType.TYPE_NUMBER_FLAG_DECIMAL;
    static final double EARTH_RADIUS = 6378.137; //[km]

    List<Anomaly> anomalyList = new ArrayList<>();

    Typeface unitFont;
    Typeface labelFont;
    Typeface buttonFont;

    TextView radiusLabel;
    EditText radiusEdit;
    TextView radiusUnit;

    TextView radiationLabel;
    EditText radiationEdit;
    TextView radiationUnit;

    TextView electricLabel;
    EditText electricEdit;
    TextView electricUnit;

    TextView magneticLabel;
    EditText magneticEdit;
    TextView magneticUnit;

    Button addButton;
    Button deleteButton;

    private boolean checkPermission(String[] permissions) { //funkcja do sprawdzania uprawnień
        if (permissions != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) { //odpowiednia wersja systemu
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED)
                    return false;
            }
        }
        return true; //jak sprawdzamy jakieś pozwolenia i wszystkie przejdą to jest true, inaczej false
    }
    private void requestPermission(String[] permissions) { // prosi o nadanie uprawnien:
        ActivityCompat.requestPermissions(this, permissions, 99); //99 to chyba kod lokacji
    }

    private void refreshLocation() {
        try {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "No autorisation for LastKnownLocation", Toast.LENGTH_LONG);
                return; //ten if sprawdza pozolenia odnoścnie uzyskiwania ostatniej lokacji
            }
            location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if(location == null) {
                location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                if (location == null) {
                    location = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
                }
            }
        }
        catch (SecurityException e) {
            Toast.makeText(this, "some other error", Toast.LENGTH_LONG);
            e.printStackTrace();
        }
    }
    private void updateLocation(){
        if(location!=null){
            latitude = location.getLatitude();
            longitude = location.getLongitude();
            updateMarker(latitude, longitude);
        }
        else{
            Toast.makeText(this, "No localisation acces", Toast.LENGTH_LONG);
        }
    }
    private void updateMarker(double lat, double lon){
        if (locationMarker != null) locationMarker.remove();
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(new LatLng(latitude, longitude));
        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.circ));
        if(mMap!=null){
            locationMarker = mMap.addMarker(markerOptions);
        }
    }

    private void addAnomaly(double lat, double lon, double radius, double radiation, double electric, double magnetic){

        mDbAdapter.open();
        long selectedID = isAnyAnomalySelected(anomalyList);

        if(selectedID == -999){ //dodajemy nową anomalie, bo żadna nie jest kliknięta
            if(radius>25) radius = 25;
            else if(radius<1) radius = 1;

            if(radiation>99999) radiation=99999;
            if(electric>9999) electric=9999;
            if(magnetic>999) magnetic=999;

            long baseID = mDbAdapter.saveAnomaly(lat,lon,radius,radiation,electric,magnetic);
            Anomaly anomaly = new Anomaly(lat,lon,radius,radiation,electric,magnetic, baseID);
            anomalyList.add(anomaly);
            anomaly.drawRange(mMap);
            Log.w(TAG, "new anomaly added to list with id: "+anomalyList.lastIndexOf(anomaly));
        }

        else{ //działamy na jakiejś istniejącej
            if(radius>25) radius = 25;
            else if(radius<1) radius = 1;

            if(radiation>99999) radiation=99999;
            if(electric>9999) electric=9999;
            if(magnetic>999) magnetic=999;


            mDbAdapter.updateAnomaly(anomalyList.get((int)selectedID).baseID,radius,radiation,electric,magnetic);

            anomalyList.get((int)selectedID).changeParameters(radius,radiation,electric,magnetic); //updatuje parametry w liście
            anomalyList.get((int)selectedID).drawnRange.remove(); //usuwamy poprzedni zasięg
            anomalyList.get((int)selectedID).drawRange(mMap); //malujemy nowy
            Log.w(TAG, "anomaly updated with list id: "+selectedID);
        }

        mDbAdapter.close();
    }
    private void deleteAnomaly(){
        mDbAdapter.open();
        long id = isAnyAnomalySelected(anomalyList);
        if(id!=-999) {
            mDbAdapter.deleteAnomaly(anomalyList.get((int)id).baseID);
            anomalyList.get((int)id).drawnRange.remove();
            Log.w(TAG, "anomaly deleted with list id: "+id);
            anomalyList.remove((int)id);
        }
        mDbAdapter.close();
    }
    private void deselectAnomaly(Anomaly anomaly){
        if(anomaly.isSelected == 1)anomaly.toggleActive(mMap);
    }
    private long isAnyAnomalySelected(List<Anomaly> list){ //sprawdza czy któraś anomalia jest kliknięta, jak tak to zwraca jej id
        for(int i=0; i<list.size(); i++){
            if(list.get(i).isSelected==1) return i;
        }
        return -999;
    }
    private double calcDistance(double touchLat, double touchLon, double rangeLat, double rangeLon){ //śmieszna funkcja Heversine'a czy kogoś takiego
        double dLat = rangeLat*Math.PI/180 - touchLat*Math.PI/180;
        double dLon = rangeLon*Math.PI/180 - touchLon*Math.PI/180;
        double tmp = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(touchLat*Math.PI/180) * Math.cos(rangeLat*Math.PI/180) *
                        Math.sin(dLon/2) * Math.sin(dLon/2);
        double tmp2 = 2 * Math.atan2(Math.sqrt(tmp), Math.sqrt(1-tmp));
        return EARTH_RADIUS * tmp2 * 1000; //odległośc w metrach
    }
    private Anomaly getClosestAnomalyInRange(double touchLat, double touchLon, List<Anomaly> list){
        double closestR = 99;
        int closestIndex = -999;
        double r;

        for(int i=0; i<list.size(); i++){ //mało eleganckie ale powinno działać
            r = calcDistance(touchLat,touchLon,list.get(i).latitude,list.get(i).longitude);
            deselectAnomaly(list.get(i)); //odklikuje anomalię (jeśli kliknięta)

            if(r < closestR && r <= list.get(i).radius){
                closestR = r;
                closestIndex = i;
            }
        }
        if(closestIndex!=-999)return list.get(closestIndex);
        else return null;
    }
    private void renderField(){
        mDbAdapter.open();
        Cursor cursor = mDbAdapter.fetchAllActiveAnomalies();
        if (cursor.moveToFirst()){
            do{
                Anomaly anomaly = new Anomaly(cursor.getFloat(cursor.getColumnIndex("latitude")),
                        cursor.getFloat(cursor.getColumnIndex("longitude")),
                        cursor.getFloat(cursor.getColumnIndex("radius")),
                        cursor.getFloat(cursor.getColumnIndex("radiationLvl")),
                        cursor.getFloat(cursor.getColumnIndex("electricLvl")),
                        cursor.getFloat(cursor.getColumnIndex("magneticLvl")),
                        cursor.getInt(cursor.getColumnIndex("id")));
                anomaly.drawRange(mMap);
                anomalyList.add(anomaly);
            }while(cursor.moveToNext());
        }
        cursor.close();
        mDbAdapter.close();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dev);

        //region przyciski i kontrolki
        radiusLabel = findViewById(R.id.RadiusLabel);
        radiusEdit = findViewById(R.id.RadiusEdit);
        radiusUnit = findViewById(R.id.RadiusUnit);

        radiationLabel = findViewById(R.id.RadiationLabel);
        radiationEdit = findViewById(R.id.RadiationEdit);
        radiationUnit = findViewById(R.id.RadiationUnit);

        electricLabel= findViewById(R.id.ElectricLabel);
        electricEdit = findViewById(R.id.ElectricEdit);
        electricUnit = findViewById(R.id.ElectricUnit);

        magneticLabel= findViewById(R.id.MagneticLabel);
        magneticEdit = findViewById(R.id.MagneticEdit);
        magneticUnit = findViewById(R.id.MagneticUnit);

        addButton= findViewById(R.id.AddButton);
        deleteButton= findViewById(R.id.DeleteButton);

        unitFont = ResourcesCompat.getFont(this, R.font.alarm_clock);
        labelFont = ResourcesCompat.getFont(this, R.font.display);
        buttonFont = ResourcesCompat.getFont(this, R.font.bellfortroughdemo);


        radiusLabel.setTypeface(labelFont);
        radiusEdit.setInputType(INPUT_TYPE);
        radiusEdit.setTypeface(labelFont);
        radiusEdit.setTextColor(ContextCompat.getColor(this, R.color.redTransparent));
        radiusUnit.setTypeface(unitFont);

        radiationLabel.setTypeface(labelFont);
        radiationEdit.setInputType(INPUT_TYPE);
        radiationEdit.setTypeface(labelFont);
        radiationEdit.setTextColor(ContextCompat.getColor(this, R.color.redTransparent));
        radiationUnit.setTypeface(unitFont);

        electricLabel.setTypeface(labelFont);
        electricEdit.setInputType(INPUT_TYPE);
        electricEdit.setTypeface(labelFont);
        electricEdit.setTextColor(ContextCompat.getColor(this, R.color.redTransparent));
        electricUnit.setTypeface(unitFont);


        magneticLabel.setTypeface(labelFont);
        magneticEdit.setInputType(INPUT_TYPE);
        magneticEdit.setTypeface(labelFont);
        magneticEdit.setTextColor(ContextCompat.getColor(this, R.color.redTransparent));
        magneticUnit.setTypeface(unitFont);

        addButton.setTypeface(buttonFont);
        deleteButton.setTypeface(buttonFont);
        //endregion, poprawić potem jak coś

        //region mapa i pozwolenia
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        criteria.setPowerRequirement(Criteria.POWER_LOW);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (!checkPermission(perm)) { //proszenie i udzielanie uprawnień
            requestPermission(perm);
        }
        try {
            locationManager.requestLocationUpdates(500, (float) 1, criteria, this, null);
            refreshLocation();
            updateLocation();
        }
        catch (SecurityException e) {
            Toast.makeText(this, "No autorisation", Toast.LENGTH_LONG);
            e.printStackTrace();
        }
        //endregion

        mDbAdapter = new DbAdapter(getApplicationContext()); //baza danych

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(radiusEdit.getText().length()>0 && radiationEdit.getText().length()>0 && electricEdit.getText().length()>0 && magneticEdit.getText().length()>0){
                    addAnomaly(latitude, longitude,
                            Double.parseDouble(radiusEdit.getText().toString()),
                            Double.parseDouble(radiationEdit.getText().toString()),
                            Double.parseDouble(electricEdit.getText().toString()),
                            Double.parseDouble(magneticEdit.getText().toString()));
                }
            }
        });

        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteAnomaly();
            }
        });

    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapStyle(new MapStyleOptions(getResources().getString(R.string.mapStyle)));
        renderField();
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), zoomLevel));

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) { //wybieranie anomalii
                Anomaly closestAnomaly = getClosestAnomalyInRange(latLng.latitude, latLng.longitude, anomalyList);
                if(closestAnomaly!=null){
                    closestAnomaly.toggleActive(mMap); //"włączamy" selekcję tej jednej anomalii

                    radiusEdit.setText(Double.toString(closestAnomaly.radius));
                    radiationEdit.setText(Double.toString(closestAnomaly.radiationLvl));
                    electricEdit.setText(Double.toString(closestAnomaly.electricLvl));
                    magneticEdit.setText(Double.toString(closestAnomaly.magneticLvl));
                }
                else{
                    radiusEdit.setText("");
                    radiationEdit.setText("");
                    electricEdit.setText("");
                    magneticEdit.setText("");
                }
            }
        });

    }

    @Override
    public void onLocationChanged(Location L) {
        this.location = L;
        updateLocation();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {
        Toast.makeText(this, "No provider", Toast.LENGTH_LONG);
    }

}
