package grunwald.pum.anomalydetector;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.res.ResourcesCompat;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Typeface;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.Marker;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DetectorActivity extends AppCompatActivity implements LocationListener {
    LocationManager locationManager;
    Criteria criteria;
    Location location;
    String[] perm = new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
    DbAdapter mDbAdapter;
    List<Anomaly> anomalyList = new ArrayList<>();

    double latitude;
    double longitude;

    Button quitButton;

    TextView screenRadiationLabel;
    TextView screenElectricLabel;
    TextView screenMagneticLabel;
    TextView screenSafetyLabel;


    TextView screenRadiationDisplay;
    TextView screenElectricDisplay;
    TextView screenMagneticDisplay;
    TextView screenSafetyDisplay;

    TextView screenRadiationUnit;
    TextView screenElectricUnit;
    TextView screenMagneticUnit;

    Typeface screenFont;
    Typeface digitFont;

    MediaPlayer radiationSound;
    MediaPlayer electricSound;
    MediaPlayer magneticSound;
    int establishedRadiationThreat=0;
    int establishedElectricThreat=0;
    int establishedMagneticThreat=0;

    private boolean checkPermission(String[] permissions) {
        if (permissions != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) { //odpowiednia wersja systemu
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED)
                    return false;
            }
        }
        return true;
    }
    private void requestPermission(String[] permissions) {
        ActivityCompat.requestPermissions(this, permissions, 99);
    }

    private void refreshLocation() {
        try {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "No autorisation for LastKnownLocation", Toast.LENGTH_LONG);
                return; //ten if sprawdza pozolenia odnoścnie uzyskiwania ostatniej lokacji
            }
            location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if(location == null) {
                location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                if (location == null) {
                    location = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
                }
            }
        }
        catch (SecurityException e) {
            Toast.makeText(this, "some other error", Toast.LENGTH_LONG);
            e.printStackTrace();
        }
    }
    private void updateLocation(){
        if(location!=null){
            latitude = location.getLatitude();
            longitude = location.getLongitude();
        }
        else{
            Toast.makeText(this, "No localisation acces", Toast.LENGTH_LONG);
        }
    }

    private void renderField(){
        mDbAdapter.open();
        Cursor cursor = mDbAdapter.fetchAllActiveAnomalies();
        if (cursor.moveToFirst()){
            do{
                Anomaly anomaly = new Anomaly(cursor.getFloat(cursor.getColumnIndex("latitude")),
                        cursor.getFloat(cursor.getColumnIndex("longitude")),
                        cursor.getFloat(cursor.getColumnIndex("radius")),
                        cursor.getFloat(cursor.getColumnIndex("radiationLvl")),
                        cursor.getFloat(cursor.getColumnIndex("electricLvl")),
                        cursor.getFloat(cursor.getColumnIndex("magneticLvl")),
                        cursor.getInt(cursor.getColumnIndex("id")));
                anomalyList.add(anomaly);
            }while(cursor.moveToNext());
        }
        cursor.close();
        mDbAdapter.close();
    }
    private double calcDistance(double currentLat, double currentLon, double rangeLat, double rangeLon){
        double dLat = rangeLat*Math.PI/180 - currentLat*Math.PI/180;
        double dLon = rangeLon*Math.PI/180 - currentLon*Math.PI/180;
        double tmp = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(currentLat*Math.PI/180) * Math.cos(rangeLat*Math.PI/180) *
                        Math.sin(dLon/2) * Math.sin(dLon/2);
        double tmp2 = 2 * Math.atan2(Math.sqrt(tmp), Math.sqrt(1-tmp));
        return DevActivity.EARTH_RADIUS * tmp2 * 1000; //odległośc w metrach
    }
    private void detectorMeasurement(double currentLat, double currentLon, List<Anomaly> list){
        double distance;
        Double radiationLvl = 0.0;
        Double electricLvl = 0.0;
        Double magneticLvl = 0.0;
        int radiationThreat=0;
        int electricThreat=0;
        int magneticThreat=0;
        for(int i=0; i<list.size(); i++){
            distance = calcDistance(currentLat,currentLon,list.get(i).latitude,list.get(i).longitude);

            if(distance <= list.get(i).radius){
                radiationLvl += anomalyList.get(i).measurementValue(distance, anomalyList.get(i).radiationLvl);
                electricLvl += anomalyList.get(i).measurementValue(distance, anomalyList.get(i).electricLvl);
                magneticLvl += anomalyList.get(i).measurementValue(distance, anomalyList.get(i).magneticLvl);
            }
        }

        screenRadiationDisplay.setText(String.format("%.0f",radiationLvl));
        screenElectricDisplay.setText(String.format("%.1f",electricLvl));
        screenMagneticDisplay.setText(String.format("%.2f",magneticLvl));

        //ustalanie stopnia zagrożenia

        if(radiationLvl>=3000) radiationThreat = 4;
        else if(radiationLvl>1000) radiationThreat = 3;
        else if(radiationLvl>500) radiationThreat = 2;
        else if(radiationLvl>20) radiationThreat = 1;
        else  radiationThreat = 0;

        if(electricLvl>=25) electricThreat = 2;
        else if(electricLvl>10) electricThreat = 1;
        else electricThreat = 0;

        if(magneticLvl>=10) magneticThreat = 2;
        else if(magneticLvl>1) magneticThreat = 1;
        else magneticThreat = 0;

        if(radiationThreat==4 || electricThreat==2 || magneticThreat==2){
            screenSafetyDisplay.setText("CRITICAL");
        }
        else if(radiationThreat==2 || radiationThreat==3 ||electricThreat==1 || magneticThreat==1){
            screenSafetyDisplay.setText("danger");
        }
        else{
            screenSafetyDisplay.setText("OK");
        }
       soundWarning(radiationThreat,electricThreat,magneticThreat);

    }

    public void soundWarning(int radiationThreat, int electricThreat, int magneticThreat){ //paskudne ale działa
        String path;
        float volume;

        //radiacja
        if(radiationSound!=null){
            if (radiationSound.isPlaying() && establishedRadiationThreat!=radiationThreat) {
                establishedRadiationThreat = radiationThreat;
                muteWarning(radiationSound);
            }
            if(!radiationSound.isPlaying() && radiationThreat>0){
                establishedRadiationThreat = radiationThreat;
                if(radiationThreat==4) {
                    path =  "android.resource://"+getPackageName()+"/raw/radlvl4";
                    volume = (float)0.9;
                }
                else if(radiationThreat==3){
                    path =  "android.resource://"+getPackageName()+"/raw/radlvl3";
                    volume = (float)0.7;
                }
                else if(radiationThreat==2){
                    path =  "android.resource://"+getPackageName()+"/raw/radlvl2";
                    volume = (float)0.5;
                }
                else{
                    path =  "android.resource://"+getPackageName()+"/raw/radlvl1";
                    volume = (float)0.3;
                }
                radiationSound.setVolume(volume,volume);
                try {
                    radiationSound.reset();
                    radiationSound.setDataSource(this, Uri.parse(path));
                    radiationSound.setLooping(true);
                    radiationSound.prepareAsync();
                    radiationSound.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        public void onPrepared(MediaPlayer mp) {
                            mp.start();
                        }
                    });
                }
                catch (Exception e) {
                    e.printStackTrace();
                    Log.e("soundWarning-radiation","exception");
                }
            }
        }

        //E-field
        if(electricSound!=null){
            if (electricSound.isPlaying() && establishedElectricThreat!=electricThreat) {
                establishedElectricThreat = electricThreat;
                muteWarning(electricSound);
            }
            if(!electricSound.isPlaying() && electricThreat>0){
                establishedElectricThreat = electricThreat;
                if(electricThreat==2) {
                    path =  "android.resource://"+getPackageName()+"/raw/eleclvl2";
                    volume = (float)0.8;
                }
                else{
                    path =  "android.resource://"+getPackageName()+"/raw/eleclvl1";
                    volume = (float)0.6;
                }

                electricSound.setVolume(volume,volume);

                try {
                    electricSound.reset();
                    electricSound.setDataSource(this, Uri.parse(path));
                    electricSound.setLooping(true);
                    electricSound.prepareAsync();
                    electricSound.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        public void onPrepared(MediaPlayer mp) {
                            mp.start();
                        }
                    });
                }
                catch (Exception e) {
                    e.printStackTrace();
                    Log.e("soundWarning-electric","exception");
                }
            }
        }

        //M-field
        if(magneticSound!=null){
            if (magneticSound.isPlaying() && establishedMagneticThreat!=magneticThreat) {
                establishedMagneticThreat = magneticThreat;
                muteWarning(magneticSound);
            }
            if(!magneticSound.isPlaying() && magneticThreat>0){
                if(magneticThreat==2) {
                    path =  "android.resource://"+getPackageName()+"/raw/magnlvl2";
                    volume = (float)0.8;
                }
                else{
                    path =  "android.resource://"+getPackageName()+"/raw/magnlvl1";
                    volume = (float)0.6;
                }
                magneticSound.setVolume(volume,volume);
                try {
                    magneticSound.reset();
                    magneticSound.setDataSource(this, Uri.parse(path));
                    magneticSound.prepareAsync();
                    magneticSound.setLooping(true);
                    magneticSound.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        public void onPrepared(MediaPlayer mp) {
                            mp.start();
                        }
                    });
                }
                catch (Exception e) {
                    e.printStackTrace();
                    Log.e("soundWarning-magnetic","exception");
                }
            }
        }
    }

    public void muteWarning(MediaPlayer mp){
        if(mp.isPlaying())
        {
            mp.stop();
        }
        try {
            mp.reset();
            mp.setDataSource("android.resource://"+getPackageName()+"/raw/silence");
            mp.prepareAsync();
            mp.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                public void onPrepared(MediaPlayer mp) {
                    mp.start();
                }
            });
        }
        catch (Exception e) {
            e.printStackTrace();
            Log.e("muteWarningn","exception");
        }

    }

    public void deleteWarning(MediaPlayer mp){
        if(mp.isPlaying())
        {
            mp.stop();
            mp.reset();
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detector);

        //region kontrolki
        screenRadiationLabel = findViewById(R.id.ScreenRadiationLabel);
        screenElectricLabel = findViewById(R.id.ScreenElectricLabel);
        screenMagneticLabel = findViewById(R.id.ScreenMagneticLabel);
        screenSafetyLabel = findViewById(R.id.ScreenSafetyLabel);


        screenRadiationDisplay = findViewById(R.id.ScreenRadiationDisplay);
        screenElectricDisplay = findViewById(R.id.ScreenElectricDisplay);
        screenMagneticDisplay = findViewById(R.id.ScreenMagneticDisplay);
        screenSafetyDisplay = findViewById(R.id.ScreenSafetyDisplay);

        screenRadiationUnit = findViewById(R.id.ScreenRadiationUnit);
        screenElectricUnit = findViewById(R.id.ScreenElectricUnit);
        screenMagneticUnit = findViewById(R.id.ScreenMagneticUnit);

        screenFont = ResourcesCompat.getFont(this, R.font.arial_condensed_bold);
        digitFont = ResourcesCompat.getFont(this, R.font.display);

        screenRadiationLabel.setTypeface(screenFont);
        screenRadiationDisplay.setTypeface(digitFont);
        screenRadiationUnit.setTypeface(screenFont);

        screenElectricLabel.setTypeface(screenFont);
        screenElectricDisplay.setTypeface(digitFont);
        screenElectricUnit.setTypeface(screenFont);

        screenMagneticLabel.setTypeface(screenFont);
        screenMagneticDisplay.setTypeface(digitFont);
        screenMagneticUnit.setTypeface(screenFont);

        screenSafetyLabel.setTypeface(screenFont);
        screenSafetyDisplay.setTypeface(digitFont);

        //endregion

        //region lokalizacja
        criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        criteria.setPowerRequirement(Criteria.POWER_LOW);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (!checkPermission(perm)) requestPermission(perm);
        try {
            locationManager.requestLocationUpdates(500, (float) 1, criteria, this, null);
            refreshLocation();
            updateLocation();
        }
        catch (SecurityException e) {
            Toast.makeText(this, "No autorisation", Toast.LENGTH_LONG);
            e.printStackTrace();
        }
        //endregion

        mDbAdapter = new DbAdapter(getApplicationContext()); //baza danych
        renderField();

        //musze im coś dać przy kreacji, więc dostały pusty dźwięk
        radiationSound = MediaPlayer.create(this, Uri.parse("android.resource://"+getPackageName()+"/raw/silence"));
        electricSound = MediaPlayer.create(this, Uri.parse("android.resource://"+getPackageName()+"/raw/silence"));
        magneticSound = MediaPlayer.create(this, Uri.parse("android.resource://"+getPackageName()+"/raw/silence"));
        if(radiationSound.isPlaying())radiationSound.stop();
        if(electricSound.isPlaying())electricSound.stop();
        if(magneticSound.isPlaying())magneticSound.stop();



        quitButton = findViewById(R.id.ScreenButton);
        quitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteWarning(radiationSound);
                deleteWarning(electricSound);
                deleteWarning(magneticSound);
                finish();
            }
        });
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        View decorView = getWindow().getDecorView();
        if (hasFocus) {
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }

    @Override
    public void onLocationChanged(Location L) {
        this.location = L;
        updateLocation();
        detectorMeasurement(latitude,longitude,anomalyList);

    }

    @Override
    public void onPause() {
        super.onPause();
        deleteWarning(radiationSound);
        deleteWarning(electricSound);
        deleteWarning(magneticSound);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        radiationSound.release();
        electricSound.release();
        magneticSound.release();
        radiationSound = null;
        electricSound = null;
        magneticSound = null;
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {
        Toast.makeText(this, "No provider", Toast.LENGTH_LONG);
    }
}
