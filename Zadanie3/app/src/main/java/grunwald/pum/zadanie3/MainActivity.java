package grunwald.pum.zadanie3;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.widget.LinearLayout;

import java.util.List;

public class MainActivity extends AppCompatActivity implements FirstFragment.OverviewFragmentActivityListener, SecondFragment.OverviewFragmentActivityListener {

    LinearLayout layout1;
    LinearLayout layout2;
    FirstFragment firstFragment;
    SecondFragment secondFragment;
    SensorManager sMgr;
    Intent service;
    BroadcastReceiver receiver;
    int statusService=0; //zmienna do podglądania statusu serwisu

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        layout1=findViewById(R.id.layout1);
        layout2=findViewById(R.id.layout2);

        //Patrzymy czy z jakichś powodów mamy dodany jakiś phantomowy fragment i jeśli tak to go usuwamy
        Fragment a = getSupportFragmentManager().findFragmentByTag("first");
        if(a != null) getSupportFragmentManager().beginTransaction().remove(a).commit();

        //Sprawdzamy w jakiej pozycji znajduje sie telefon
        int orientation = getResources().getConfiguration().orientation;

        if(orientation== Configuration.ORIENTATION_LANDSCAPE){ //jeśli mamy telefon poziomo...
            if(firstFragment == null){ //...oraz nie mamy stworzonego FirstFragment...
                firstFragment = FirstFragment.newInstance(); //.. tworzymy go
                getSupportFragmentManager().beginTransaction().add(R.id.layout1, firstFragment, "first").commit(); // i wstawiamy w ukał

            }
            //analogicznie z drugim
            if(secondFragment == null){
                secondFragment = SecondFragment.newInstance();
                getSupportFragmentManager().beginTransaction().add(R.id.layout2, secondFragment, "second").commit();

            }
        }
        else if(orientation== Configuration.ORIENTATION_PORTRAIT){//jeśli mamy telefon pionowo...
            if(firstFragment == null){ //...oraz nie mamy stworzonego FirstFragment...
                firstFragment = FirstFragment.newInstance(); //.. tworzymy go
                getSupportFragmentManager().beginTransaction().add(R.id.layout1, firstFragment, "first").commit(); // i wstawiamy w ukał

            }
        }
        //sensor Manager
        sMgr = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

        //service
        service = new Intent(getBaseContext(), MyService.class);
        receiver = new BroadcastReceiver() { //dzieki niemu możemy pobierać wartosci z usługi i ustawic tekst w drugim fragmencie
            public void onReceive(Context context, Intent intent) {
                String txt = intent.getStringExtra("data");
                secondFragment.setText(txt);
            }
        };

    }

    @Override
    public void onClickSensorList() {
        List<Sensor> list = sMgr.getSensorList(Sensor.TYPE_ALL); //pobieramy listę sensorów z telefonu
        StringBuilder data = new StringBuilder(); //chyba taki dziwny string
        for(Sensor sensor : list) {
            data.append(sensor.getName() + "\n");
            data.append(sensor.getVendor() + "\n");
            data.append(sensor.getVersion() + "\n");
        }
        int orientation = getResources().getConfiguration().orientation;

        if(orientation==Configuration.ORIENTATION_LANDSCAPE){
            SecondFragment.setText(data.toString());
        }
        else if(orientation== Configuration.ORIENTATION_PORTRAIT) {
            if (getSupportFragmentManager().findFragmentByTag("first")!=null) {
                getSupportFragmentManager().beginTransaction().remove(getSupportFragmentManager().findFragmentByTag("first")).commit();
                secondFragment = secondFragment.newInstance(data.toString());
                getSupportFragmentManager().beginTransaction().add(R.id.layout1, secondFragment, "first").commit(); // i wstawiamy w układ

            }
        }

    }

    @Override
    public void onClickService() {
        //trochę dublujemy onClickSensorList, to tak powinno być?
        int orientation = getResources().getConfiguration().orientation;
        if(orientation== Configuration.ORIENTATION_PORTRAIT) {
            if (getSupportFragmentManager().findFragmentByTag("first")!=null) {
                getSupportFragmentManager().beginTransaction().remove(getSupportFragmentManager().findFragmentByTag("first")).commit();
            }
                secondFragment = secondFragment.newInstance("Uruchomiono usługę...");
                getSupportFragmentManager().beginTransaction().add(R.id.layout1, secondFragment, "first").commit(); // i wstawiamy w układ
        }
        //co robimy w zalezności od statusu
        if(statusService==0){
            startService(service);
            statusService=1;
        }
        else if(statusService==1){
            if(orientation==Configuration.ORIENTATION_LANDSCAPE) {
                secondFragment.setText("");
            }
            statusService=0;
            stopService(service);
        }
    }

    @Override
    protected void onStart() { //tworzy intentFilter i ustawia broadcastReceiver
        super.onStart();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("fizyka.pum.sensory");
        registerReceiver(receiver, intentFilter); //dodaje nasz receiver
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(receiver); //wyklucza nasz receiver
    }

    @Override
    public void onClickBackButton() {
        int orientation = getResources().getConfiguration().orientation;
        if(statusService==0){
            if(orientation==Configuration.ORIENTATION_LANDSCAPE){
                secondFragment.setText("");
            }
            else if(orientation== Configuration.ORIENTATION_PORTRAIT) {
                if (getSupportFragmentManager().findFragmentByTag("first") != null) {
                    getSupportFragmentManager().beginTransaction().remove(getSupportFragmentManager().findFragmentByTag("first")).commit();
                    firstFragment = FirstFragment.newInstance();
                    getSupportFragmentManager().beginTransaction().add(R.id.layout1, firstFragment, "first").commit(); // i wstawiamy w układ

                }
            }
        }
        else if(statusService==1){
            statusService=0;
            stopService(service);
        }
    }
}
