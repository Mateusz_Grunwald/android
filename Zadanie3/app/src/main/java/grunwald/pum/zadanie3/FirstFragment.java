package grunwald.pum.zadanie3;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class FirstFragment extends Fragment {

    OverviewFragmentActivityListener listener;
    public View mView;
    Button sensorButton;
    Button serviceButton;

    public FirstFragment() {
        // Required empty public constructor
    }

    public interface OverviewFragmentActivityListener{
        void onClickSensorList();
        void onClickService();
    }

    class onClickListener implements View.OnClickListener { //klasa tego cutomowego słuchacza
        @Override
        public void onClick(View v) {
            //sprawdzamy co klikneliśmy i w zależności od tego wywołujemy odpowiedną funkcję z interfaceu
            int id = v.getId();
            if (id == R.id.sensorButton) listener.onClickSensorList();
            else if (id == R.id.serviceButton) listener.onClickService();
            else System.out.println("something fucked up, dunno");
        }
    }


    public static FirstFragment newInstance() {
        FirstFragment fragment = new FirstFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_first, container, false);
        listener = (OverviewFragmentActivityListener) inflater.getContext();

        sensorButton = mView.findViewById(R.id.sensorButton);

        serviceButton = mView.findViewById(R.id.serviceButton);

        onClickListener buttonListener = new onClickListener(); //ten listener do przycisków
        sensorButton.setOnClickListener(buttonListener);
        serviceButton.setOnClickListener(buttonListener);
        return mView;
    }

}
