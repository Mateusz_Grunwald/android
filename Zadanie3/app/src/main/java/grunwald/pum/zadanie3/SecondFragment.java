package grunwald.pum.zadanie3;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;


public class SecondFragment extends Fragment {

    OverviewFragmentActivityListener listener;
    View mView;
    static TextView tView; //textView w scrollview, czyli ta nasza śmieszna scrollowana lista
    Button backButton;

    //zmienne do ustawiania tekstu w tView
    public static String text;
    public static Boolean setText;



    public SecondFragment() {
        // Required empty public constructor
    }

    public interface OverviewFragmentActivityListener{
        void onClickBackButton();
    }

    public static void setText(String txt){
        tView.setText(txt);
    }

    public static SecondFragment newInstance() {
        SecondFragment fragment = new SecondFragment();
        text = "";
        setText = false;
        return fragment;
    }
    public static SecondFragment newInstance(String txt) {
        SecondFragment fragment = new SecondFragment();
        text = txt;
        setText = true;
        return fragment;
    }

    class onClickListener implements View.OnClickListener { //klasa tego cutomowego słuchacza
        @Override
        public void onClick(View v) {
            //sprawdzamy co klikneliśmy i w zależności od tego wywołujemy odpowiedną funkcję z interfaceu
            int id = v.getId();
            if (id == R.id.returnButton) listener.onClickBackButton();
            else System.out.println("something fucked up, dunno");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_second, container, false);
        tView = mView.findViewById(R.id.sensorList);
        listener = (OverviewFragmentActivityListener) inflater.getContext();

        backButton = mView.findViewById(R.id.returnButton);
        onClickListener buttonListener = new onClickListener(); //ten listener do przycisków
        backButton.setOnClickListener(buttonListener);
        if(setText) tView.setText(text);

        return mView;
    }
}
