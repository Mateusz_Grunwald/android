package grunwald.pum.zadanie3;

import android.app.Service;
import android.content.Intent;
import android.icu.text.SimpleDateFormat;
import android.os.Build;
import android.os.IBinder;
import android.widget.Toast;

import androidx.annotation.RequiresApi;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class MyService extends Service {
    Timer timer;
    TimerTask timerTask;
    public MyService() {
    }

    public class MyTimerTask extends TimerTask{

        @RequiresApi(api = Build.VERSION_CODES.N)
        @Override
        public void run() {
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()); //pobieramy datę
            //Tworzymy nowa intencje, dzieki której wysyłamy timeStamp:
            Intent intent1 = new Intent();
            intent1.setAction("fizyka.pum.sensory");
            intent1.putExtra("data",timeStamp + "\n");
            sendBroadcast(intent1);
        }
    }

    //trochę magicznych funkcji
    public MyTimerTask initTask(){
        MyTimerTask mTT = new MyTimerTask();
        return mTT;
    }

    public void clearTimerSchedule(){
        if(timerTask!=null){
            timerTask.cancel();
            timer.purge();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        timer.cancel();
        timerTask.cancel();
        Toast.makeText(getApplicationContext(),"rzuciliśmy onDestroy",Toast.LENGTH_LONG).show();
    }
   public int onStartCommand(Intent intent, int flags, int startId){
        clearTimerSchedule();
        timerTask = initTask();
        timer.scheduleAtFixedRate(timerTask, 1000, 1000);
        return START_STICKY; //system spróbuje odtworzyć intencję jeśli tylko zostanie z jakiegoś powodu zniszczona
   }

    @Override
    public void onCreate() {
        super.onCreate();
        timer = new Timer();
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
